import { INITIAL_MOCK_DATA } from './todo-data'
import uuidv4 from 'uuid/v4'

let repository = Array.from(INITIAL_MOCK_DATA)

const findAll = () => Promise.resolve(repository)

const findById = id => {
  let index = repository.findIndex(dbItem => dbItem.id === id)

  if (index > -1) {
    return Promise.resolve(repository[index])
  }

  return Promise.reject
}

const add = item => {
  const uuid = uuidv4()
  let todoItem = { ...item, id: uuid }
  repository.push(todoItem)
  return Promise.resolve(todoItem)
}

const update = item => {
  let index = repository.findIndex(dbItem => dbItem.id === item.id)

  if (index > -1) {
    repository[index] = item
    return Promise.resolve(item)
  }

  return Promise.reject()
}

export const todoService = {
  findAll,
  findById,
  add,
  update
}
