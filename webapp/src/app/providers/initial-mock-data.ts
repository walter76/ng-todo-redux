import { TodoItem } from '../models/todo-item.model'

export const INITIAL_MOCK_DATA: TodoItem[] = [
  {
    id: 'e0daca4d-4c8f-46f1-b518-70ddec6ea714',
    description: 'This item has still to be finished.',
    done: false
  },
  {
    id: '3cbc8ea2-72b4-4f58-8a6d-359cfb52e7fa',
    description: 'A second item that has not been finished yet.',
    done: false
  },
  {
    id: 'b40a1c9b-c1d3-4039-96f9-5df869711846',
    description: 'This item has been finished.',
    done: true
  },
  {
    id: '5d0df4c3-a29b-4e70-a514-bd627750d782',
    description: 'Another item, that has been finished.',
    done: true
  },
  {
    id: 'e72ae886-75c8-4d29-ae46-46ba5da43514',
    description: 'A third item that the owner did not work on yet.',
    done: false
  }
]
