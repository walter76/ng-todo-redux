import express from 'express'
import bodyParser from 'body-parser'

import { registerTodoRoutes } from './services/todo/routes'

const PORT = 8080
const app = express()

app.use(bodyParser.json())

registerTodoRoutes(app)

app.listen(PORT, () =>
  console.log(`server running on http://localhost:${PORT}`)
)
