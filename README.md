# NgTodoRedux

This is a example application that I have developed to get more insights on Angular Development. As most of the toy
applications it is a very simple use case: handling a list of todos.

The application itself has a frontend and a backend part.

## Backend

We need a directory for the backend and have to initialize it:

```
mkdir backend
cd backend
yarn init -y
```

The last command is running `yarn` to initialize the backend. With the option `-y` the tool is using the default
settings.

### Setup Babel, Eslint and Prettier

```
yarn add babel-cli babel-eslint babel-preset-env babel-preset-stage-2 -D
```

```json
{
  "name": "backend",
  "version": "0.0.1",
  "main": "index.js",
  "license": "MIT",
  "repository": "git@gitlab.com:walter76/ng-todo-redux.git",
  "author": "walter <wrstocke@googlemail.com>",
  "scripts": {
    "start": "babel-node -- src/index.js"
  },
  "devDependencies": {
    "babel-cli": "^6.26.0",
    "babel-eslint": "^10.0.1",
    "babel-preset-env": "^1.7.0",
    "babel-preset-stage-2": "^6.24.1"
  }
}
```

```json
{
  "presets": [ "env", "stage-2" ]
}
```

```
yarn add eslint eslint-config-prettier eslint-plugin-import eslint-plugin-prettier prettier -D
```

```json
{
  "extends": ["prettier"],
  "plugins": ["prettier", "import"],
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 2016,
    "sourceType": "module"
  },
  "env": {
    "es6": true,
    "browser": true,
    "node": true
  }
}
```

### Use express

```
yarn add express
```

```js
import express from 'express'

const PORT = 8080
const app = express()

app.get('/*', (_req, res) => res.send('<div>Hello, Express!</div>'))

app.listen(PORT, () =>
  console.log(`server running on http://localhost:${PORT}`)
)
```

```
yarn start
```

![](doc/assets/backend-express-simple.png)

### Setup Testing

```
yarn add jasmine -D
node node_modules/jasmine/bin/jasmine init
```

### Add a service for handling Todos

```
yarn add body-parser
```

```js
export const INITIAL_MOCK_DATA = [
  {
    id: 'e0daca4d-4c8f-46f1-b518-70ddec6ea714',
    description: 'This item has still to be finished.',
    done: false
  },
  {
    id: '3cbc8ea2-72b4-4f58-8a6d-359cfb52e7fa',
    description: 'A second item that has not been finished yet.',
    done: false
  },
  {
    id: 'b40a1c9b-c1d3-4039-96f9-5df869711846',
    description: 'This item has been finished.',
    done: true
  },
  {
    id: '5d0df4c3-a29b-4e70-a514-bd627750d782',
    description: 'Another item, that has been finished.',
    done: true
  },
  {
    id: 'e72ae886-75c8-4d29-ae46-46ba5da43514',
    description: 'A third item that the owner did not work on yet.',
    done: false
  }
]
```

```js
import { INITIAL_MOCK_DATA } from './todo-data'

export const todoService = {
  findAll: () => INITIAL_MOCK_DATA
}
```

```js
import { todoService } from './todo-service'

const get = (_req, res) => res.send(todoService.findAll())

export const registerTodoRoutes = app => app.get('/api/todo', get)
```

```js
import express from 'express'
import bodyParser from 'body-parser'

import { registerTodoRoutes } from './services/todo/routes'

const PORT = 8080
const app = express()

app.use(bodyParser.json())

registerTodoRoutes(app)

app.listen(PORT, () =>
  console.log(`server running on http://localhost:${PORT}`)
)
```

```
yarn start
```

http://localhost:8080/api/todo

![](doc/assets/backend-todo-service-1.png)

## Client Web Application

Create the client web application with Angular CLI:

```
ng new ng-todo-redux --style=scss --directory webapp
cd webapp
yarn
```

### References

* https://www.sitepoint.com/ultimate-angular-cli-reference/
* https://blog.angular-university.io/how-to-build-angular2-apps-using-rxjs-observable-data-services-pitfalls-to-avoid/
* https://blog.oasisdigital.com/2017/angular-runtime-performance-guide/

### Material for Styling

We use [Material](https://material.angular.io/) for styling. We are going to use this to add it to our project:

```
yarn add @angular/material @angular/cdk @angular/animations
```

You need to add some code to `styles.css` to enable the theme and fonts:

```css
@import '~@angular/material/prebuilt-themes/deeppurple-amber.css';
@import url('https://fonts.googleapis.com/css?family=Roboto');
```

There are other seems. I prefer the `deeppurple-amber.css`.

Next you need to adapt your `app.module.ts` to enable animations:

```ts
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

@NgModule({
  ...
  imports: [BrowserAnimationsModule],
  ...
})
export class AppModule {}
```

Every single component that is needed has to be imported in the `AppModule`. See more details about that in the
[getting-started](https://material.angular.io/guide/getting-started)

### Create a List of Todo Items

`src/app/app.component.html`:

```html
<mat-toolbar color="primary"><span>Dead-Simple ToDo Application</span></mat-toolbar>

<mat-card>
  <mat-card-header>
    <mat-card-title>My List of Things To Do</mat-card-title>
  </mat-card-header>
  <mat-card-content>
    <mat-list>
      <mat-list-item *ngFor="let todoItem of todoItems">
        <h4 mat-line>{{todoItem.description}}</h4>
      </mat-list-item>
    </mat-list>
  </mat-card-content>
</mat-card>
```

`src/styles.css`:

```css
body {
  margin: 0;
  font-family: Roboto, sans-serif;
}

mat-card {
  max-width: 80%;
  margin: 2em auto;
}
```

`src/app/models/todo-item.model.ts`:

```ts
export interface TodoItem {
  id: string
  description: string
  done: boolean
}
```

`src/app/providers/initial-mock-data.ts`:

```ts
import { TodoItem } from '../models/todo-item.model'

export const INITIAL_MOCK_DATA: TodoItem[] = [
  {
    id: 'e0daca4d-4c8f-46f1-b518-70ddec6ea714',
    description: 'This item has still to be finished.',
    done: false
  },
  {
    id: '3cbc8ea2-72b4-4f58-8a6d-359cfb52e7fa',
    description: 'A second item that has not been finished yet.',
    done: false
  },
  {
    id: 'b40a1c9b-c1d3-4039-96f9-5df869711846',
    description: 'This item has been finished.',
    done: true
  },
  {
    id: '5d0df4c3-a29b-4e70-a514-bd627750d782',
    description: 'Another item, that has been finished.',
    done: true
  },
  {
    id: 'e72ae886-75c8-4d29-ae46-46ba5da43514',
    description: 'A third item that the owner did not work on yet.',
    done: false
  }
]
```

`src/app/app.component.ts`:

```ts
import { INITIAL_MOCK_DATA } from './providers/initial-mock-data'
import { Component } from '@angular/core'
import { TodoItem } from './models/todo-item.model'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  todoItems: TodoItem[] = INITIAL_MOCK_DATA
}
```

![](doc/assets/list-of-todo-items-first-shot.png)

```
yarn add @angular/flex-layout
```

```html
<mat-toolbar color="primary"><span>Dead-Simple ToDo Application</span></mat-toolbar>

<mat-card>
  <mat-card-header>
    <mat-card-title>My List of Things To Do</mat-card-title>
    <mat-card-subtitle>All the things I haven't finished yet...</mat-card-subtitle>
  </mat-card-header>
  <mat-card-content>
    <div fxLayout="column">
      <div fxFlex *ngFor="let todoItem of todoItems">
        <h4>{{todoItem.description}}</h4>
      </div>
    </div>
  </mat-card-content>
</mat-card>
```

```ts
import { NgModule } from '@angular/core'

import { MatToolbarModule } from '@angular/material/toolbar'
import { MatCardModule } from '@angular/material/card'

@NgModule({
  imports: [MatToolbarModule, MatCardModule],
  exports: [MatToolbarModule, MatCardModule]
})
export class MaterialModule {}
```

```ts
import { MaterialModule } from './material.module'

@NgModule({
  imports: [
    MaterialModule
  ]
})
export class AppModule {}
```

```
ng g s providers/todo-item
```

```ts
import { Injectable } from '@angular/core'

import { INITIAL_MOCK_DATA } from './initial-mock-data'
import { TodoItem } from '../models/todo-item.model'

@Injectable({
  providedIn: 'root'
})
export class TodoItemService {
  getAll(): Promise<TodoItem[]> {
    return Promise.resolve(INITIAL_MOCK_DATA)
  }
}
```

```ts
import { Component } from '@angular/core'
import { TodoItem } from './models/todo-item.model'
import { TodoItemService } from './providers/todo-item.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  todoItems: TodoItem[] = []

  constructor(private todoItemService: TodoItemService) {
    this.todoItemService
      .getAll()
      .then(todoItemList => (this.todoItems = todoItemList))
  }
}
```

Introduce [SASS](https://sass-lang.com/) by renaming all files with `.scss`. There are two syntaxes, the newer one
`.scss` and the older one `.sass`. We go with the newer one. Be aware that you have to adapt `angular.json` for the new
name `styles.scss`.

You need to restart `yarn start` after renaming.