import { Pipe, PipeTransform } from '@angular/core'

import { TodoItem } from './models/todo-item.model'

@Pipe({ name: 'doneFilter' })
export class DoneFilterPipe implements PipeTransform {
  transform(todoItems: TodoItem[], done: boolean): TodoItem[] {
    return todoItems.filter(item => item.done === done)
  }
}
