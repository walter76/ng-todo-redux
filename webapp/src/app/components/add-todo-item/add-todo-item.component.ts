import { Component, Output, EventEmitter } from '@angular/core'

@Component({
  selector: 'app-add-todo-item',
  templateUrl: './add-todo-item.component.html',
  styleUrls: ['./add-todo-item.component.css']
})
export class AddTodoItemComponent {
  @Output()
  add = new EventEmitter<string>()

  constructor() {}

  addItem(description: string): boolean {
    this.add.emit(description)

    return false
  }
}
