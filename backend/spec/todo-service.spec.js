import { todoService } from '../src/services/todo/todo-service'

describe('add an item', () => {
  it('should add an item', done => {
    todoService
      .add({ description: 'some item', done: false })
      .then(addedTodo => {
        expect(addedTodo.description).toBe('some item')
        expect(addedTodo.done).toBe(false)
        expect(todoService.findById(addedTodo.id)).toBeDefined()
        done()
      })
  })
})
