import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core'
import { FlexLayoutModule } from '@angular/flex-layout'

import { MaterialModule } from './material.module'

import { AppComponent } from './app.component'
import { TodoItemComponent } from './components/todo-item/todo-item.component'
import { AddTodoItemComponent } from './components/add-todo-item/add-todo-item.component'
import { DoneFilterPipe } from './done-filter.pipe'

@NgModule({
  declarations: [
    AppComponent,
    TodoItemComponent,
    AddTodoItemComponent,
    DoneFilterPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
