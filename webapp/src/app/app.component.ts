import { Component } from '@angular/core'
import { TodoItem } from './models/todo-item.model'
import { TodoItemService } from './providers/todo-item.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  todoItems: TodoItem[] = []

  constructor(private todoItemService: TodoItemService) {
    this.todoItemService
      .getAll()
      .then(todoItemList => (this.todoItems = todoItemList.slice(0)))
  }

  onItemDone(index: number): void {
    this.todoItemService
      .update({ ...this.todoItems[index], done: true })
      .then(updatedTodo => (this.todoItems[index] = updatedTodo))
      .catch(() => console.error(`unable to update item ${index}`))
  }

  addItem(description: string): void {
    console.log(`adding new item ${description}`)

    this.todoItemService
      .add({ id: '', description, done: false })
      .then(todoItem => this.todoItems.push(todoItem))
  }
}
