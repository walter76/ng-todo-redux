import { Component, Input, Output, EventEmitter } from '@angular/core'

import { TodoItem } from '../../models/todo-item.model'

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent {
  @Input()
  todoItem: TodoItem
  @Output()
  done = new EventEmitter<boolean>()

  constructor() {}

  onItemDone(): void {
    this.done.emit(true)
    this.todoItem.done = true
  }
}
