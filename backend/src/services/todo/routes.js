import { todoService } from './todo-service'

const get = (_req, res) => todoService.findAll().then(items => res.send(items))

const getById = (req, res) =>
  todoService
    .findById(req.params.id)
    .then(item => res.json(item))
    .catch(() => res.status(404))

const create = (req, res) =>
  todoService.add(req.body).then(item => res.json(item))

const update = (req, res) => {
  todoService
    .update(req.body)
    .then(item => res.json(item))
    .catch(() => res.status(404))
}

export const registerTodoRoutes = app => {
  app.get('/api/todo', get)
  app.get('/api/todo/:id', getById)
  app.post('/api/todo', create)
  app.put('/api/todo', update)
}
