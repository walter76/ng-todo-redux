import { Injectable } from '@angular/core'

import { v4 as uuid } from 'uuid'

import { INITIAL_MOCK_DATA } from './initial-mock-data'
import { TodoItem } from '../models/todo-item.model'

@Injectable({
  providedIn: 'root'
})
export class TodoItemService {
  repository: TodoItem[] = INITIAL_MOCK_DATA

  getAll(): Promise<TodoItem[]> {
    return Promise.resolve(this.repository)
  }

  update(item: TodoItem): Promise<TodoItem> {
    console.log(`updating item ${item.id}`)
    console.table(item)

    let index = this.repository.findIndex(todo => todo.id === item.id)

    console.log(`index is ${index}`)

    if (index < 0) {
      return Promise.reject()
    }

    this.repository[index] = item
    return Promise.resolve(item)
  }

  add(item: TodoItem): Promise<TodoItem> {
    console.log(`adding new item`)
    console.table(item)

    this.repository.push({ ...item, id: uuid() })

    return Promise.resolve(this.repository[this.repository.length - 1])
  }
}
